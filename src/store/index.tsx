import { createStore, applyMiddleware } from "redux";
import { createEpicMiddleware } from "redux-observable";
import { composeWithDevTools } from "redux-devtools-extension";
import rootReducer from "reducers";
import { rootEpic } from '../epics';

// tu przydałoby się typowanie, ogólnie tu będzie dużo do zmiany, jeżeli można tu dać od razu tego tokena
// to tylko szablon jak wykorzystać te epiki w storze

export const configureStore = () => {
  const epicMiddleware = createEpicMiddleware();

  const store = createStore(
    rootReducer,
    /* preloadedState, */
    composeWithDevTools(applyMiddleware(epicMiddleware))
  );
  epicMiddleware.run(rootEpic);


  return store;
};


