import React, { Suspense, } from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './App';
import * as serviceWorker from './serviceWorker';
// import i18n (needs to be bundled ;))
import './utils/index';
import { Provider } from 'react-redux';
import { configureStore } from 'store/index';


// loading component for suspense fallback
const Loader = () => (
  <div className="App">
    <div>loading...</div>
  </div>
);


ReactDOM.render(
    <Provider store={configureStore()}>
      <Suspense fallback={<Loader/>}>
        <App/>
      </Suspense>
    </Provider>
    ,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
