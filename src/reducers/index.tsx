import { combineReducers } from "redux";
import { AUTH_SUCCESS } from "actions";

interface IUserID {
  userID: string;
}

const authInitialState: IUserID = { userID: "" };

const authReducer = (
  state = authInitialState,
  action: { type: string; id: string }
) => {
  switch (action.type) {
    case AUTH_SUCCESS:
      return {
        ...state,
        userID: action.id,
      };
    // tutaj można tego Failure wsadzić
    default:
      return state;
  }
};

// można tworzyć obiekty, żeby potem wykorzystywać w komponencie, w tym przypadku auth
const rootReducer = combineReducers({
  auth: authReducer,
  // kolejne reducery
});

export default rootReducer;
