import React from 'react';
import LoginPage from 'components/pages/loginPage';

function App() {
  return (
    <div>
      <LoginPage/>
    </div>
  );
}

export default App;
