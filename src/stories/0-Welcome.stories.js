import React from 'react';
import Input from 'components/atoms/input';

export default {
  component: Input,
  title: 'Input',
};

export const text = () => <Input type='text' placeholder='test'/>;
export const email = () => <Input type='email' placeholder='test'/>;
export const password = () => <Input type='password' placeholder='test'/>;
