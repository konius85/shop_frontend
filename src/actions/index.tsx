// te stałe powinniśmy trzymać w osobnym pliku, dla przykładu na razie są tu
export const AUTH_REQUEST = 'AUTH_REQUEST';
export const AUTH_SUCCESS = 'AUTH_SUCCESS';
// akcję failure można tutaj dodać wg naszego uznania, ale jezeli dobrze pamiętam, Failure z epików może iść
// prosto reducera i tam można stworzyć case, żeby store dostał ten błąd
export const AUTH_FAILURE = 'AUTH_FAILURE';

// taką akcję wykorzystuje się w hooku dispatch, w komponencie
export const authenticateRequest = (username: string, password: string) => ({
  type: AUTH_REQUEST,
  username,
  password,
});

// to ID jest zwrotką z serwera - ID użytkownika
export const authenticateSuccess = (id: string) => ({
  type: AUTH_SUCCESS,
  id,
});
