export interface IInput {
  label?: string;
  isRounded?: boolean;
  disabled?: boolean;
  placeholder?: string;
  type?: EInputType,
}

export enum EInputType {
  Text = 'text',
  Password = 'password',
  Email = 'email',
  Tel = 'tel',
  Submit = 'submit',
  Reset = 'reset'
}
