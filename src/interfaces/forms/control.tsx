import { ReactNode } from "react";

export interface IFormControlBase {
  children: ReactNode;
}

export interface IFormControl {
  isLoading?: boolean;
  iconLeft?: string;
  iconRight?: string;
}
