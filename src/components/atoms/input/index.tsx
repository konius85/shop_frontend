import React from 'react';
import classNames from 'classnames';
import { EInputType, IInput } from 'interfaces/forms/input';
import { useTranslation } from 'react-i18next';


export default function Input(props: IInput) {
  const {t} = useTranslation();

  return (
    <input
      disabled={props.disabled ?? false}
      placeholder={t(`${props.placeholder}`)}
      type={props.type ?? EInputType.Text}
      className={classNames({
        input: true,
        'is-rounded': props.isRounded ?? false,
      })}
    />
  );
}
