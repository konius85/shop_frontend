import React from 'react';
import { useTranslation } from 'react-i18next';

export default function Label(props: { label: string }) {
  const {t} = useTranslation();

  return (
    <label className="label">{t(`${props.label}`)}</label>
  );
}
