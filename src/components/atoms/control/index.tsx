import React from 'react';
import classNames from 'classnames';
import { IFormControl, IFormControlBase } from 'interfaces/forms/control';


export default function Control(props: IFormControl & IFormControlBase) {
  const iconLeft = props.iconLeft;
  const iconRight = props.iconRight;

  return (
    <div
      className={classNames({
        control: true,
        'is-loading': props.isLoading ?? false,
        'has-icons-left': iconLeft,
        'has-icons-right': iconRight,
      })}
    >
      {props.children}
      {iconLeft &&
      <span className="icon is-small is-left">
          <i className={`fas fa-${iconLeft}`}></i>
        </span>
      }
      {iconRight &&
      <span className="icon is-small is-right">
          <i className={`fas fa-${iconRight}`}></i>
        </span>
      }
    </div>
  );
}
