import React from 'react';
import InputControl from 'components/molecules/form/inputControl';
import { EInputType } from 'interfaces/forms/input';
import { useTranslation } from 'react-i18next';
import './styles.scss';
import { useDispatch } from 'react-redux';
import { AUTH_REQUEST } from 'actions/index';

export default function LoginPage() {
  const {t} = useTranslation();
  const dispatch = useDispatch();


  const submitForm = () => {
    dispatch({ type: AUTH_REQUEST, username: 'test', password: 'test' })
  };

  return (
  <div className={'wrapper'}>
      <InputControl placeholder='loginPage.email' iconLeft='at' label='loginPage.email'/>
      <InputControl label='loginPage.password' type={EInputType.Password} placeholder='loginPage.password'
                    iconLeft='key'/>
      <button className='button is-primary' onClick={submitForm}>{t('loginPage.send')}</button>
  </div>
  );
}
