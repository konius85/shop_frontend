import React from 'react';
import Control from 'components/atoms/control';
import { IInput } from 'interfaces/forms/input';
import Input from 'components/atoms/input';
import { IFormControl } from 'interfaces/forms/control';
import Label from 'components/atoms/label';


export default function InputControl(props: IInput & IFormControl) {
  return (
    <div className='field'>
      {props.label && <Label label={props.label}/>}
      <Control iconLeft={props.iconLeft} iconRight={props.iconRight} isLoading={props.isLoading}>
        <Input type={props.type} disabled={props.disabled}
               placeholder={props.placeholder}/>
      </Control>
    </div>
  );
}
