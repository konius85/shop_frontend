import { ActionsObservable, combineEpics, ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import axios from 'axios';
import { AUTH_FAILURE, AUTH_REQUEST, authenticateSuccess } from 'actions';

// interface AuthenticateAction {
//   type: string;
//   username: string;
//   password: string;
// }
console.log(`${process.env.REACT_APP_API_URL}/login`);

const authenticateEpic = (action$: ActionsObservable<any>) =>
  action$.pipe(
    ofType(AUTH_REQUEST),
    switchMap(
      ({username, password}: { username: string; password: string }) =>
        from(
          axios.post(`${process.env.REACT_APP_API_URL}/login`, {
            username,
            password,
          })
        ).pipe(
          tap(data => console.log("data", data)),
          map((response) => authenticateSuccess(response.data)),
          catchError((error) => {
            console.log("ERROR", error)
              return of({type: AUTH_FAILURE, payload: error})
          }
          )
        )
    )
  );

export const rootEpic = combineEpics(
  authenticateEpic
  // kolejne epiki
);
